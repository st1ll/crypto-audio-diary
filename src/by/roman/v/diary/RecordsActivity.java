package by.roman.v.diary;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.commonsware.cwac.endless.EndlessAdapter;

import by.roman.v.diary.adapter.EndlessAdapterImpl;
import by.roman.v.diary.db.entity.Record;
import by.roman.v.diary.service.DbService;
import by.roman.v.diary.service.PlayService;
import android.R.integer;
import android.app.ListActivity;
import android.content.Context;
import android.media.AudioManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.view.ContextMenu.ContextMenuInfo;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.ListView;

public class RecordsActivity extends ListActivity {

	private ListActivity activity;
	private PlayService playService;
	private DbService dbService;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.activity = this;
		playService = new PlayService((AudioManager) getSystemService(Context.AUDIO_SERVICE));
		dbService = DbService.getInstance();
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		EndlessAdapter adapter = new EndlessAdapterImpl(new ArrayList<Record>(), activity);
		setListAdapter(adapter);
		adapter.notifyDataSetChanged();
		registerForContextMenu(getListView());
	}
	
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		menu.setHeaderTitle(getString(R.string.context_menu_actions));
		menu.add(0, v.getId(), 0, R.string.context_menu_delete);
	}
	
	@Override
	public boolean onContextItemSelected(MenuItem item) {
		AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
		Record record = (Record) getListAdapter().getItem(info.position);
		if (item.getGroupId() == 0) {
			playService.stop();
			deleteRecord(record);
			setListAdapter(new EndlessAdapterImpl(new ArrayList<Record>(), activity));
			((EndlessAdapter) getListAdapter()).notifyDataSetChanged();
			return true;
		}
		return super.onContextItemSelected(item);
	}
	
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		Record record = (Record) getListAdapter().getItem(position);
		playService.play(record.getPath());
	}
	
	private void deleteRecord(Record record) {
		dbService.deleteRecord(record.getId());
		File file = new File(record.getPath());
		file.delete();
	}
	
	@Override
	public void onBackPressed() {
		playService.stop();
		super.onBackPressed();
	}
}
