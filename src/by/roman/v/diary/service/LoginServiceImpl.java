package by.roman.v.diary.service;

import java.sql.SQLException;
import java.util.List;

import android.content.Context;
import android.util.Log;
import by.roman.v.diary.R;
import by.roman.v.diary.db.entity.Password;
import by.roman.v.diary.db.helper.DbHelper;

public class LoginServiceImpl implements LoginService {
	
	private static LoginServiceImpl instance;
	
	private DbHelper helper;
	private Context context;
	private String tempPassword;
	
	public static void init(Context context) {
		instance = new LoginServiceImpl(context);
	}
	
	private LoginServiceImpl(Context context) {
		helper = new DbHelper(context);
		this.context = context;
	}
	
	public static LoginServiceImpl getInstance() {
		if (instance == null) {
			Log.e(LoginServiceImpl.class.getName(), "LoginServiceImpl not initialized, envoke init(context) first");
			throw new RuntimeException();
		}
		return instance;
	}

	public boolean isRegistered() {
		List<Password> result = null;
		try {
			result = helper.getPasswordDao().queryForAll();
		} catch (SQLException e) {
			Log.e(LoginServiceImpl.class.getName(), "Can`t query encrypted word", e);
			throw new RuntimeException(e);
		}
		return (result == null) ? false : !result.isEmpty();
	}

	public boolean isValid(String password) {
		List<Password> result = null;
		try {
			result = helper.getPasswordDao().queryForAll();
		} catch (SQLException e) {
			Log.e(LoginServiceImpl.class.getName(), "Can`t query encrypted word", e);
			throw new RuntimeException(e);
		}
		if (result == null || result.isEmpty()) {
			Log.e(LoginServiceImpl.class.getName(), "Password isnt registered");
			throw new RuntimeException();
		}
		Password encryptedWordHolder = result.get(0);
		String encryptedWord = encryptedWordHolder.getEncryptedWord();
		try {
			SimpleCrypto.decrypt(password, encryptedWord);
		} catch (Exception e) {
			Log.e(LoginServiceImpl.class.getName(), "Decrypt secret word exeption", e);
			return false;
		}
		return true;
	}

	public void setTempPassword(String password) {
		this.tempPassword = password;
	}

	public String getTempPassword() {
		return tempPassword;
	}

	public void register(String password) {
		this.tempPassword = password;
		String originalSecretWord = getOriginalSecretWord();
		String encryptedWord = null;
		try {
			encryptedWord = SimpleCrypto.encrypt(password, originalSecretWord);
		} catch (Exception e) {
			Log.e(LoginServiceImpl.class.getName(), "Encrypt secret word exeption", e);
			throw new RuntimeException(e);
		}
		Password passwordEntity = new Password(encryptedWord);
		try {
			helper.getPasswordDao().create(passwordEntity);
		} catch (SQLException e) {
			Log.e(LoginServiceImpl.class.getName(), "Can`t save secret word in DB", e);
			throw new RuntimeException(e);
		}

	}
	
	private String getOriginalSecretWord() {
		String originalSecretWord = context.getString(R.string.secret_word);
		if (originalSecretWord == null || "".equals(originalSecretWord)) {
			Log.e(LoginServiceImpl.class.getName(), "Can`t get original secret word from strings");
			throw new RuntimeException();
		}
		return originalSecretWord;
	}

}
