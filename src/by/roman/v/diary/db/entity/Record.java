package by.roman.v.diary.db.entity;

import java.util.Date;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable
public class Record {
	
	@DatabaseField(generatedId = true)
	private Long id;
	
	@DatabaseField
	private Date date;
	
	@DatabaseField
	private String path;
	
	@DatabaseField
	private String comment;
	
	@DatabaseField
	private boolean favorites;
	
	public Record() {
	};
	
	public Record(String path) {
		this.path = path;
		this.date = new Date();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public boolean isFavorites() {
		return favorites;
	}

	public void setFavorites(boolean favorites) {
		this.favorites = favorites;
	}
}
