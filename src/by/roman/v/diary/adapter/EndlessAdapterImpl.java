package by.roman.v.diary.adapter;

import java.util.List;

import android.app.Activity;
import android.app.ListActivity;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import by.roman.v.diary.R;
import by.roman.v.diary.db.entity.Record;
import by.roman.v.diary.service.DbService;

import com.commonsware.cwac.endless.EndlessAdapter;

public class EndlessAdapterImpl extends EndlessAdapter {
	
	private ListActivity activity;
	private RotateAnimation rotate = null;
	private List<Record> list = null;
	private List<Record> prevList = null;
	private static final String PATTERN = "d MMMM yyyy kk:mm";

	public EndlessAdapterImpl(List<Record> list, ListActivity activity) {
		super(new ArrayAdapter<Record>(activity, R.layout.row, android.R.id.text1, list));
		this.activity = activity;
		this.prevList = list;
		
		rotate = new RotateAnimation(0f, 360f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF,
                0.5f);
        rotate.setDuration(600);
        rotate.setRepeatMode(Animation.RESTART);
        rotate.setRepeatCount(Animation.INFINITE);

	}
	
	@Override
	protected View getPendingView(ViewGroup parent) {
		View row = ((Activity) activity).getLayoutInflater().inflate(R.layout.row, null);

        View child = row.findViewById(android.R.id.text1);

        child.setVisibility(View.GONE);

        child = row.findViewById(R.id.throbber);
        child.setVisibility(View.VISIBLE);
        child.startAnimation(rotate);

        return (row);

	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		PlaceHolder holder = null;
        if (convertView == null) {
            holder = new PlaceHolder();
            LayoutInflater inflater = ((Activity) activity).getLayoutInflater();
            View view = inflater.inflate(R.layout.row, parent, false);
            convertView = view;
            holder.label = (TextView) view.findViewById(android.R.id.text1);
            holder.day = (TextView) view.findViewById(R.id.dayName);
            
            view.setTag(holder);
        } else {
            holder = (PlaceHolder) convertView.getTag();
        }
        int count = prevList.size();
        if (position < count && count > 0) {
            Record element = (Record) prevList.get(position);
            holder.label.setText(DateFormat.format(PATTERN, element.getDate()));
            holder.day.setVisibility(TextView.GONE);
            if (position != 0) {
            	Record prevRecord = (Record) prevList.get(position - 1);
            	if (prevRecord.getDate().getDay() != element.getDate().getDay()) {
            		holder.day.setText(DateFormat.format("EEEE", element.getDate()));
            		holder.day.setVisibility(TextView.VISIBLE);
            	}
            } else {
            	holder.day.setText(DateFormat.format("EEEE", element.getDate()));
        		holder.day.setVisibility(TextView.VISIBLE);
            }
        } else {
            return super.getView(position, convertView, parent);
        }
        return convertView;

	}
	
	@Override
	protected void appendCachedData() {
        prevList.addAll(list);
        list.clear();
	}

	@Override
	protected boolean cacheInBackground() throws Exception {
        DbService dbService = DbService.getInstance();
        list = dbService.getPortion(10, prevList.size());
        return !list.isEmpty();

	}
	
	private class PlaceHolder {
        public TextView label;
        public TextView day;
    }


}
