package by.roman.v.diary.service;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

public class CryptoService {

	private static String TEMP;

	public static void init(String cacheDir) {
		TEMP = cacheDir + "/tempDiary.record";
	}

	public static void encrypt(String path) {
		try {
			File source = new File(path);
			BufferedInputStream inputStream = new BufferedInputStream(new FileInputStream(source));
			byte[] buffer = new byte[(int) source.length()];
			inputStream.read(buffer);
			inputStream.close();
			String seed = LoginServiceImpl.getInstance().getTempPassword();
			byte[] encryptedBuffer = SimpleCrypto.encrypt(seed, buffer);
			BufferedOutputStream output = new BufferedOutputStream(new FileOutputStream(source));
			output.write(encryptedBuffer);
			output.flush();
		} catch (FileNotFoundException e) {
			Log.e(CryptoService.class.getName(), "Can`t read file " + path, e);
			throw new RuntimeException(e);
		} catch (IOException e) {
			Log.e(CryptoService.class.getName(), "Can`t read file " + path, e);
			throw new RuntimeException(e);
		} catch (Exception e) {
			Log.e(CryptoService.class.getName(), "Can`t encrypt file " + path, e);
			throw new RuntimeException(e);
		}
		
	}
	
	public static String decrypt(String path) {
		try {
			File source = new File(path);
			BufferedInputStream inputStream = new BufferedInputStream(new FileInputStream(source));
			byte[] buffer = new byte[(int) source.length()];
			inputStream.read(buffer);
			inputStream.close();
			String seed = LoginServiceImpl.getInstance().getTempPassword();
			byte[] encryptedBuffer = SimpleCrypto.decrypt(seed, buffer);
			File temp = new File(TEMP);
			BufferedOutputStream output = new BufferedOutputStream(new FileOutputStream(temp));
			output.write(encryptedBuffer);
			output.flush();
			output.close();
			return TEMP;
		} catch (FileNotFoundException e) {
			Log.e(CryptoService.class.getName(), "Can`t read file " + path, e);
			throw new RuntimeException(e);
		} catch (IOException e) {
			Log.e(CryptoService.class.getName(), "Can`t read file " + path, e);
			throw new RuntimeException(e);
		} catch (Exception e) {
			Log.e(CryptoService.class.getName(), "Can`t decrypt file " + path, e);
			throw new RuntimeException(e);
		}
		
	}
}
