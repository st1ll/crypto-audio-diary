package by.roman.v.diary;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.UUID;

import com.smaato.soma.BannerView;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.AvoidXfermode;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import by.roman.v.diary.service.CryptoService;
import by.roman.v.diary.service.DbService;
import by.roman.v.diary.service.PlayService;
import by.roman.v.diary.service.RecordService;

public class MainActivity extends Activity implements OnClickListener {
	
	private RecordService recordService;
	private static final String FOLDER = "by.roman.v.diary";
	private static final String PATH = Environment.getExternalStorageDirectory().getAbsoluteFile() + "/" + FOLDER;
	RotateAnimation rotate;
	private ImageView recAnim;
	private Dialog aboutDialog;
	private static final String COUNT_PARAM = "launch_count";
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        recordService = new RecordService();
        CryptoService.init(PATH);
        DbService.init(this);
        File folder = new File(PATH);
        folder.mkdir();
        setContentView(R.layout.activity_main);
        Button listButton = (Button) findViewById(R.id.listButton);
        listButton.setOnClickListener(this);
        Button aboutButton = (Button) findViewById(R.id.aboutButton);
        aboutButton.setOnClickListener(this);
        
        rotate = new RotateAnimation(0f, 360f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF,
                0.5f);
        rotate.setDuration(1600);
        rotate.setRepeatMode(Animation.RESTART);
        rotate.setRepeatCount(Animation.INFINITE);
        recAnim = (ImageView) findViewById(R.id.recAnim);
        recAnim.setOnClickListener(this);
        aboutDialog = new AboutDialog(this);
        
        SharedPreferences settings = getSharedPreferences("diary.info", MODE_PRIVATE);
        Integer count = settings.getInt(COUNT_PARAM, 0);
        SharedPreferences.Editor editor = settings.edit();
        count++;
        editor.putInt(COUNT_PARAM, count);
        editor.commit();
        
        if (count == 3) {
        	aboutDialog.show();
        }

        
    }
    
    @Override
    protected void onResume() {
    	super.onResume();
    	BannerView mBanner = (BannerView) findViewById(R.id.bannerView); 
    	mBanner.getAdSettings().setPublisherId(923864343);
    	mBanner.getAdSettings().setAdspaceId(65767912);
		mBanner.getAdSettings().setAdType(com.smaato.soma.AdType.ALL);
		mBanner.asyncLoadNewBanner();
		mBanner.setAutoReloadEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }

	public void onClick(View view) {
		String tag = view.getTag().toString();
		if ("list".equals(tag)) {
			Intent intent = new Intent();
			intent.setClass(this, RecordsActivity.class);
			startActivity(intent);
		}
		if ("imageRec".equals(tag)) {
			if (recordService.isRecording()) {
				stop();
			} else {
				record();
			}
			return;
		}
		if ("about".equals(tag)) {
			aboutDialog.show();
			return;
		}
	}
	
	private void record() {
		String name = PATH + "/" + (UUID.randomUUID()) + ".record";
		Log.i(getClass().getName(), name);
		recordService.record(name);
		recAnim.startAnimation(rotate);
	}
	
	private void stop() {
		recordService.stop();
		recAnim.clearAnimation();
	}
	
	@Override
	public void onBackPressed() {
		recordService.stop();
		super.onBackPressed();
	}
	
	private String readRawTextFile(int id) {
		InputStream inputStream = getResources().openRawResource(id);
		InputStreamReader in = new InputStreamReader(inputStream);
		BufferedReader buf = new BufferedReader(in);
		String line;
		StringBuilder text = new StringBuilder();
		try {
			while (( line = buf.readLine()) != null) text.append(line);
		} catch (IOException e) {
			return null;
		}
		
		return text.toString();
	}
	

	

    
}
