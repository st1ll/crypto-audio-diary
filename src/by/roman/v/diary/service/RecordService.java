package by.roman.v.diary.service;

import java.io.IOException;

import android.media.MediaRecorder;

public class RecordService {

	private MediaRecorder recorder = null;
	private String path;
	
	public void record(String outputPath) {
		if (recorder != null) {
			return;
		}
		this.path = outputPath;
		recorder = new MediaRecorder();
		recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
		recorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
		recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
		recorder.setAudioEncodingBitRate(16);
		recorder.setAudioSamplingRate(44100);
		recorder.setOutputFile(outputPath);
		try {
			recorder.prepare();
		} catch (IOException e) {
			e.printStackTrace();
		}
		recorder.start();
	}
	
	public void stop() {
		if (recorder != null) {
			recorder.stop();
			recorder.release();
			recorder = null;
			DbService.getInstance().createRecord(getPath());
			CryptoService.encrypt(getPath());
		}
	}
	
	public String getPath() {
		return path;
	}
	
	public boolean isRecording() {
		if (recorder != null) {
			return true;
		}
		return false;
	}
}
