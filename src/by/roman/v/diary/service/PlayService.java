package by.roman.v.diary.service;

import java.io.File;
import java.io.IOException;

import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnInfoListener;

public class PlayService implements OnInfoListener, OnCompletionListener {
	
	private MediaPlayer player = null;
	private String path;
	
	public PlayService(AudioManager audioManager) {
        audioManager.setMode(AudioManager.MODE_NORMAL);
        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, 20, 0);
	}
	
	public void play(String encryptedPath) {
		stop();
		path = CryptoService.decrypt(encryptedPath);
		player = new MediaPlayer();
		player.setVolume(100, 100);
		try {
			player.setDataSource(path);
			player.prepare();
		} catch (IOException e) {
			e.printStackTrace();
		}
		player.start();
		player.setOnInfoListener(this);
		player.setOnCompletionListener(this);
	}
	
	public void stop() {
		if (player == null || !player.isPlaying()) {
			return;
		}
		player.stop();
		destroy();
	}
	
	private void destroy() {
		player.release();
		player = null;
		File temp = new File(path);
		temp.delete();
		temp = null;
	}
	
	public boolean onInfo(MediaPlayer mp, int what, int extra) {
		if (mp.isPlaying() == false) {
			destroy();
		}
		return true;
	}

	public void onCompletion(MediaPlayer mp) {
		destroy();
	}

}
