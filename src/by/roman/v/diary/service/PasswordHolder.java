package by.roman.v.diary.service;

import android.util.Log;

public class PasswordHolder {

	private static PasswordHolder instance;
	private static String password;
	
	private PasswordHolder() {
	}
	
	public static void init(String pass) {
		password = pass; 
	}
	
	public static PasswordHolder getInstance() {
		if (instance == null) {
			Log.e(PasswordHolder.class.getName(), "PasswordHolder not initialized");
			throw new RuntimeException();
		}
		return instance;
	}

	public String getPassword() {
		return password;
	}
}
