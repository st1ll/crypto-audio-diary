package by.roman.v.diary.service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;

import android.content.Context;
import android.util.Log;
import by.roman.v.diary.db.entity.Record;
import by.roman.v.diary.db.helper.DbHelper;

public class DbService {
	
	private static DbService instance;
	private final DbHelper helper;
	
	private DbService(Context context) {
		helper = new DbHelper(context);
	}
	
	public static void init(Context context) {
		instance = new DbService(context);
	}
	
	public static DbService getInstance() {
		if (instance == null) {
			Log.e(DbService.class.getName(), "DbService not initialized");
			throw new RuntimeException();
		}
		return instance;
	}
	
	public void createRecord(String path) {
		Record record = new Record(path);
		try {
			helper.getRecordDao().create(record);
		} catch (SQLException e) {
			Log.e(DbService.class.getName(), "Can`t create Record", e);
			throw new RuntimeException(e);
		}
	}
	
	public void deleteRecord(Long id) {
		try {
			helper.getRecordDao().deleteById(id);
		} catch (SQLException e) {
			Log.e(DbService.class.getName(), "Can`t delete Record", e);
			throw new RuntimeException(e);
		}
	}
	
	public List<Record> getRecords() {
		List<Record> result = new ArrayList<Record>();
		try {
			result = helper.getRecordDao().queryForAll();
		} catch (SQLException e) {
			Log.e(DbService.class.getName(), "Can`t query all Records", e);
			throw new RuntimeException(e);
		}
		return result;
	}
	
	public List<Record> getPortion(int size, int offset) {
		List<Record> result = new ArrayList<Record>();
		try {
			QueryBuilder<Record, Long> builder = helper.getRecordDao().queryBuilder();
			builder.limit(size);
			builder.offset(offset);
			builder.orderBy("date", false);
			builder.prepare();
			result = builder.query();
		} catch (SQLException e) {
			Log.e(DbService.class.getName(), "Can`t query Records", e);
			throw new RuntimeException(e);
		}
		return result;
	}
}
