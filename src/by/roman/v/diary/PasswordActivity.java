package by.roman.v.diary;

import by.roman.v.diary.service.LoginService;
import by.roman.v.diary.service.LoginServiceImpl;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * �������� �� ���� ������ (layout- pass_layout)
 * @author Roman V
 *
 */
public class PasswordActivity extends Activity{

	/**
	 * ������ ��� ����� ������
	 */
	private EditText passField;
	private LoginService loginService;
	private int registerTryNumber = 0;
	private String tempPassword = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.pass_layout);
		LoginServiceImpl.init(this);
		loginService = LoginServiceImpl.getInstance();
		passField = (EditText) findViewById(R.id.passField);
		Button loginButton = (Button) findViewById(R.id.loginButton);
		Button registerButton = (Button) findViewById(R.id.registerButton);
		
		if (loginService.isRegistered()) {
			return;
		} else {
			loginButton.setVisibility(Button.INVISIBLE);
			registerButton.setVisibility(Button.VISIBLE);
		}
	}
	
	/**
	 * ����� ������� ��� ������� onClick ��� ���� ����� ������
	 * @param view - ������
	 */
	public void onNumClick(View view) {
		if (!(view instanceof Button)) {
			Log.e(PasswordActivity.class.getName(), "Password num button handler error - view is not button");
			return;
		}
		Button button = (Button) view;
		CharSequence text = button.getText();
		passField.append(text);
		passField.setError(null);
	}
	
	/**
	 * ����� �������� �� ������ Login pass_layout
	 * @param view - ������ Login
	 */
	public void onLogin(View view) {
		if (!(view instanceof Button)) {
			Log.e(PasswordActivity.class.getName(), "Password login handler error - view is not a Button");
			return;
		}
		String password = passField.getText().toString();
		passField.setText("");
		
		if (!isNotEmpty(password)) {
			return;
		}
		
		if (loginService.isValid(password)) {
			loginService.setTempPassword(password);
			goNext();
			return;
		} else {
			passField.setError(getString(R.string.pass_wrong));
			return;
		}
		
	}
	
	public void onRegister(View view) {
		if (!(view instanceof Button)) {
			Log.e(PasswordActivity.class.getName(), "Password register handler error - view is not a Button");
			return;
		}
		
		String password = passField.getText().toString();
		passField.setText("");
		if (!isNotEmpty(password)) {
			tempPassword = null;
			registerTryNumber = 0;
			Toast.makeText(this, getString(R.string.register_new_password), Toast.LENGTH_SHORT).show();
			return;
		}
		
		if (tempPassword == null && registerTryNumber == 0) {
			tempPassword = password;
			registerTryNumber++;
			Toast.makeText(this, getString(R.string.register_confirm), Toast.LENGTH_SHORT).show();
			return;
		}
		
		if (password.equals(tempPassword) && registerTryNumber == 1) {
			loginService.register(password);
			Toast.makeText(this, getString(R.string.register_success), Toast.LENGTH_SHORT).show();
			goNext();
			return;
		} else {
			tempPassword = null;
			registerTryNumber = 0;
			Toast.makeText(this, getString(R.string.register_new_password), Toast.LENGTH_SHORT).show();
			return;
		}
	}
	
	private boolean isNotEmpty(String password) {
		if (password == null || "".equals(password)) {
			passField.setError(getString(R.string.pass_validation));
			return false;
		}
		return true;
	}
	
	private void goNext() {
		Intent intent = new Intent();
		intent.setClass(this, MainActivity.class);
		startActivity(intent);
	}
	
}
