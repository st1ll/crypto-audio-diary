package by.roman.v.diary.db.entity;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable
public class Password {

	@DatabaseField(generatedId = true)
	private Long id;
	
	@DatabaseField
	private String encryptedWord;
	
	public Password() {
	}
	
	public Password(String encryptedWord) {
		this.encryptedWord = encryptedWord;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEncryptedWord() {
		return encryptedWord;
	}

	public void setEncryptedWord(String encryptedWord) {
		this.encryptedWord = encryptedWord;
	}
}
