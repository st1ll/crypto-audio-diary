package by.roman.v.diary.db.helper;

import java.sql.SQLException;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import by.roman.v.diary.db.entity.Password;
import by.roman.v.diary.db.entity.Record;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

public class DbHelper extends OrmLiteSqliteOpenHelper {

	private static final String DATABASE_NAME = "ByRomanVDiary.sqlite";
	private static final int DATABASE_VERSION = 2;
	private Dao<Record, Long> recordDao = null;
	private Dao<Password, Long> passwordDao = null;
	
	public DbHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}
	
	@Override
	public void onCreate(SQLiteDatabase arg0, ConnectionSource connectionSource) {
		try {
			TableUtils.createTable(connectionSource, Record.class);
			TableUtils.createTable(connectionSource, Password.class);
		} catch (SQLException e) {
			Log.e(DbHelper.class.getName(), "Can`t create database", e);
			throw new RuntimeException(e);
		}

	}

	@Override
	public void onUpgrade(SQLiteDatabase arg0, ConnectionSource connectionSource, int arg2,
			int arg3) {
		try {
			TableUtils.dropTable(connectionSource, Record.class, true);
			TableUtils.dropTable(connectionSource, Password.class, true);
			onCreate(arg0, connectionSource);
		} catch (SQLException e) {
			Log.e(DbHelper.class.getName(), "Can`t upgrade database", e);
			throw new RuntimeException(e);
		}

	}
	
	@SuppressWarnings("unchecked")
	public Dao<Record, Long> getRecordDao() {
		if (recordDao == null) {
			try {
				recordDao = getDao(Record.class);
			} catch (SQLException e) {
				Log.e(DbHelper.class.getName(), "Can`t get recordDao", e);
				throw new RuntimeException(e);
			}
		}
		return recordDao;
	}
	
	public Dao<Password, Long> getPasswordDao() {
		if (passwordDao == null) {
			try {
				passwordDao = getDao(Password.class);
			} catch (SQLException e) {
				Log.e(DbHelper.class.getName(), "Can`t get passwordDao", e);
				throw new RuntimeException(e);
			}
		}
		return passwordDao;
	}

}
