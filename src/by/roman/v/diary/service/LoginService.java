package by.roman.v.diary.service;

public interface LoginService {
	
	boolean isRegistered();
	
	boolean isValid(String password);
	
	void setTempPassword(String password);
	
	String getTempPassword();
	
	void register(String password);
}
